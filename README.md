Grew as-a-service
=======

Grew as-a-service is a parser for sentences served through a REST API.
It is able to compute surface syntax as well as deep syntax.
It is based on Grew (http://grew.fr)

## Build

`make`

## Launch the parser server

`./grew_main.native`

It runs by default on port 8092.

## Send a query to the parser

```
curl -H "Content-Type: application/json" -X POST -d '{
 "data":{"query":"la/DET/le souris/NC/souris a/V/avoir \u00e9t\u00e9/VPP/\u00eatre mang\u00e9/VPP/manger par/P/par le/DET/le chat/NC/chat"},
 "type":"query",
 "service":"deep_syntax"
 }' http://localhost:8092
```

## Response

```
{
  "service":"deep_syntax",
  "type":"response",
  "hostname":"ycouillec-vdi.vm.vdi.s1.p.fti.net",
  "timestamp":"2018-01-10T10:29:39.778388",
  "status":"success",
  "data":{
    "query":"la/DET/le souris/NC/souris a/V/avoir \u00e9t\u00e9/VPP/\u00eatre mang\u00e9/VPP/manger par/P/par le/DET/le chat/NC/chat",
    "answer":[
      {"id":"1","form":"la","lemma":"le","upos":"D","xpos":"DET","feats":[],"deps":[{"head":"2","deprel":"det"}]},
      {"id":"2","form":"souris","lemma":"souris","upos":"N","xpos":"NC","feats":[{"feature":"s","value":"c"}],"deps":[{"head":"5","deprel":"suj"}]},
      {"id":"3","form":"a","lemma":"avoir","upos":"V","xpos":"V","feats":[{"feature":"dl","value":"avoir"},{"feature":"m","value":"ind"}],"deps":[{"head":"5","deprel":"aux.tps"}]},
      {"id":"4","form":"\u00e9t\u00e9","lemma":"\u00eatre","upos":"V","xpos":"VPP","feats":[{"feature":"dl","value":"\u00eatre"},{"feature":"dm","value":"pastpart"},{"feature":"m","value":"part"},{"feature":"t","value":"past"}],"deps":[{"head":"5","deprel":"aux.pass"}]},
      {"id":"5","form":"mang\u00e9","lemma":"manger","upos":"V","xpos":"VPP","feats":[{"feature":"diat","value":"passif"},{"feature":"dl","value":"manger"},{"feature":"dm","value":"pastpart"},{"feature":"m","value":"part"},{"feature":"t","value":"past"}],"deps":[]},
      {"id":"6","form":"par","lemma":"par","upos":"P","xpos":"P","feats":[],"deps":[{"head":"5","deprel":"p_obj.agt"}]},
      {"id":"7","form":"le","lemma":"le","upos":"D","xpos":"DET","feats":[],"deps":[{"head":"8","deprel":"det"}]},
      {"id":"8","form":"chat","lemma":"chat","upos":"N","xpos":"NC","feats":[{"feature":"s","value":"c"}],"deps":[{"head":"6","deprel":"obj.p"}]}
    ]
  }
}
```

which corresponds to the following graph

![](http://grew.fr/deep_syntax/test.deep.svg)