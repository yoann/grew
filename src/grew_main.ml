(***********************************************************************)
(*    Grew - a Graph Rewriting tool dedicated to NLP applications      *)
(*                                                                     *)
(*    Copyright 2011-2013 Inria, Université de Lorraine                *)
(*                                                                     *)
(*    Webpage: http://grew.loria.fr                                    *)
(*    License: CeCILL (see LICENSE folder or "http://www.cecill.info") *)
(*    Authors: see AUTHORS file                                        *)
(***********************************************************************)

open Printf
open Log
open Conll
open Unix
open Yojson.Basic.Util

open Libgrew

open Grew_args
open Grew_utils

let port = ref 8092
let surf_grs = ref "ressources/POStoSSQ/grs/surf_synt_main.grs"
let dsq_grs = ref "ressources/SSQtoDSQ/grs/main_dsq.grs"
let deep_grs = ref "ressources/sequoia/sequoia_proj.grs"

let apply grs_file strat =
  let grs = Grs.load grs_file in
  (fun gr ->
    match Rewrite.simple_rewrite ~gr ~grs ~strat with
    | [one] -> one
    | _ -> failwith "[appy] several graphs: not managed")

let to_surf_synt = apply !surf_grs !Grew_args.strat

let to_dsq = apply !dsq_grs !Grew_args.strat

let to_deep_syntax = apply !deep_grs "deep"

let string_of_conll conll = String.concat "\n" (List.map Conll.to_string [conll])

let deep_syntax brown_line =
  let pos_graph = Graph.of_brown brown_line in
  let surf_synt = to_surf_synt pos_graph in
  let dsq = to_dsq surf_synt in
  let deep = to_deep_syntax dsq in
  print_endline (string_of_conll (Graph.to_conll surf_synt)) ;
  print_endline (string_of_conll (Graph.to_conll dsq)) ;
  print_endline (string_of_conll (Graph.to_conll deep)) ;
  deep

let string_of_time tm =
  let t = gmtime tm in
  let year = 1900 + t.tm_year in
  let month = t.tm_mon + 1 in
  let day = t.tm_mday in
  let hour = t.tm_hour in
  let min = t.tm_min in
  let sec = t.tm_sec in
  let us = int_of_float (1000000.0 *. (tm -. (float_of_int (int_of_float tm)))) in
  let syear = string_of_int year in
  let smonth = (fun s -> if month < 10 then "0"^s else s) (string_of_int month) in
  let sday = (fun s -> if day < 10 then "0"^s else s) (string_of_int day) in
  let shour = (fun s -> if hour < 10 then "0"^s else s) (string_of_int hour) in
  let smin = (fun s -> if min < 10 then "0"^s else s) (string_of_int min) in
  let ssec = (fun s -> if sec < 10 then "0"^s else s) (string_of_int sec) in
  let sus = (fun s -> if us < 10 then "0"^s else s) (string_of_int us) in
  syear^"-"^smonth^"-"^sday^"T"^shour^":"^smin^":"^ssec^"."^sus

let log msg =
  print_endline ("\r["^(string_of_time (gettimeofday()))^" "^msg^"]")

let create_server port =
  let addr = Unix.inet_addr_any in
  let saddr = Unix.ADDR_INET(addr,port) in
  let socket = Unix.socket (Unix.domain_of_sockaddr saddr) Unix.SOCK_STREAM 0 in
  Unix.bind socket saddr ;
  Unix.listen socket 100 ;
  log ("Server created on port "^(string_of_int port));
  socket

let accept_client socket =
  let client = Unix.accept socket in
  let channels = fst client in
  let addr = Unix.getnameinfo (snd client) [Unix.NI_NUMERICHOST] in
  log ("Client accepted "^(addr.Unix.ni_hostname)^":"^(addr.Unix.ni_service));
  (Unix.in_channel_of_descr channels, Unix.out_channel_of_descr channels)

let http_parse_method inc =
  let line = input_line inc in
  match String.split_on_char ' ' line with
  | [meth;path;prot] -> (meth,path,prot)
  | _ -> failwith "method line has wrong format"

let split_at_first s c =
  let i = String.index s c in
  let n = String.length s in
  let first = String.sub s 0 i in
  let second = String.sub s (i + 1) (n - i -1) in
  [first; second]

let rec http_parse_headers inc =
  let line = String.trim (input_line inc) in
  let len = String.length line in
  if len == 0 then
    []
  else
    (match split_at_first line ':' with
    | [name;value] ->
       (name,
	List.map
	  (fun e -> String.trim e)
	  (String.split_on_char ';' value))
       ::(http_parse_headers inc)
    | _ -> failwith ("header has wrong format: "^line))

let http_read_json_content inc headers =
  let len_s = List.hd (List.assoc "Content-Length" headers) in
  let len = int_of_string len_s in
  let content_type = List.assoc "Content-Type" headers in
  if List.mem "application/json" content_type then
    (let buff = Bytes.create len in
     let _ = input inc buff 0 len in
     Bytes.to_string buff)
  else
    ""

let rec json_of_conll conll =
  let lines = conll.Conll.lines in
  List.map
    (fun line -> `Assoc [
      ("id",`String (Conll.Id.to_string line.Conll.id));
      ("form",`String (line.Conll.form));
      ("lemma",`String (line.Conll.lemma));
      ("upos",`String (line.Conll.upos));
      ("xpos",`String (line.Conll.xpos));
      ("feats",`List (List.map
			(function (feature,value) ->
			  `Assoc [
			    ("feature", `String (feature));
			    ("value", `String (value));
			  ])
			line.Conll.feats));
      ("deps",`List (List.map
		       (function (head,deprel) ->
			 `Assoc [
			   ("head", `String (Conll.Id.to_string head));
			   ("deprel", `String (deprel));
			 ])
		       line.Conll.deps));
    ])
    lines

let rec service (inc, out) =
  let (_,_,_) = http_parse_method inc in
  let headers = http_parse_headers inc in
  let content = http_read_json_content inc headers in
  let json_content = Yojson.Basic.from_string content in
  let rq_type = json_content |> member "type" |> to_string in
  let service_name = json_content |> member "service" |> to_string in
  let query = json_content |> member "data" |> member "query" |>to_string in
  print_endline query ;
  if rq_type = "query" && service_name = "deep_syntax" then
    let deep = Graph.to_conll (deep_syntax query) in
    let json_deep = json_of_conll deep in
    let _ = Conll.to_dot deep in
    let json_result = `Assoc [
      ("service", `String "deep_syntax");
      ("type", `String "response");
      ("hostname", `String (Unix.gethostname()));
      ("timestamp", `String (string_of_time (Unix.gettimeofday())));
      ("status", `String "success");
      ("data", `Assoc [
	("query", `String query);
	("response",`List json_deep)])] in
    let string_result = Yojson.Basic.to_string json_result in
    output_string out "HTTP/1.1 200 OK\n" ;
    output_string out ("Content-Type:application/json\n") ;
    output_string out ("Content-Length: "^(string_of_int (String.length string_result))^"\n") ;
    output_string out ("\n") ;
    output_string out (string_result) ;
    flush out ;
    close_out out
  else
    failwith "error"

let rec accept_loop server =
  let inc, outc = accept_client server in
  ignore(Thread.create service (inc, outc)) ;
  accept_loop server

let _ =
  let options =
    [
      ("--port", Arg.Set_int port, "Port on which the application runs");
      ("--surf-grs", Arg.Set_string surf_grs, "path to the surface syntax grs");
      ("--dsq-grs", Arg.Set_string dsq_grs, "path to the dsq grs");
      ("--deep-grs", Arg.Set_string deep_grs, "path to the deep syntax grs");
    ] in
  Arg.parse options (fun _ -> ()) "Options:";
  let server = create_server !port in
  accept_loop server
